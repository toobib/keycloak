

# TOOBIB THEME

## Getting started

Any file duplicated in the custom theme from the base theme will overwrite the base file located in the default theme.

The file `template.ftl` represents the model replicated on each page; it is imported by all `.ftl` files (except the template one) with:
![import "template.ftl" as layout](../importSS.png)

The files in the `messages` folder contain all the resources called by the "nested" function in the `.ftl` files.

Resource to learn more about the structure of themes: [Customizing Keycloak Themes](https://trigodev.com/blog/how-to-customize-keycloak-themes)

Various JS scripts (such as the `passwordVisibility` script, which allows displaying or hiding the password in the input field) are present in Keycloak's base folders but can also be included in the customized theme, customized scripts are available at `pscToobib2/login/resources/scripts/rppsValidation.js`.

Most of the CSS used for this theme has been moved to the bottom of the CSS file found at `import/theme/pscToobib2/login/resources/css/psc.css`

## Example of code

Button with `passwordVisibility` function:
![Button creation(with passwordVisibility function):](../buttonSS.png)

JS file to import to access `passwordVisibility` function:
![Script to import to access passwordVisibility function:](../scriptImportSS.png)

## Other resources

- [Spring Security with Keycloak and Custom Themes](https://www.baeldung.com/spring-keycloak-custom-themes)
- [Custom Themes in Keycloak](https://www.baeldung.com/keycloak-custom-login-page)

Additionally, several options can be activated in the Keycloak administration panel.

For example, you can enable the dropdown menu to switch between languages by going to Realm Settings > Localization, activating Internationalization, and then selecting the languages that will be available on your Keycloak pages.
