document.addEventListener('DOMContentLoaded', function() {
    var form = document.getElementById('kc-register-form');
    var inputRppsId = document.getElementById('rppsId');
    var selectedLanguage = document.getElementById('kc-current-locale-link');

    // Je récupère la valeur de mon message d'erreur dans mon formulaire (register.ftl)
    var errorMessage = document.getElementById('input-error-rppsId');

    form.addEventListener('submit', function(event) {
        var inputRppsIdValue = inputRppsId.value.trim();
        var  regex = /^[0-9]+$/;

        if (!inputRppsIdValue || inputRppsIdValue.length !== 11 || !regex.test(inputRppsIdValue)) {

                // Vérification sur le language utilisé par l'utilisateur (valeur de l'attribut outerText de la variable prenant pour valeur l'id "kc-current-locale-link)
                // afin d'adapter le message d'erreur utilisé
                if(selectedLanguage.outerText === "Français"){
                    errorMessage.textContent = 'Veuillez renseigner un identifiant RPPS valide de 11 chiffres.';
                }else if(selectedLanguage.outerText === "English"){
                    errorMessage.textContent = 'Please specify a valid RPPS Identifier of 11 digits.';
                }else{
                    errorMessage.textContent = 'Please specify a valid RPPS Identifier of 11 digits.';
                }

                // Ici j'utilise le comportement d'affichage des messages d'erreur de Keycloak par défaut en mettant la valeur de l'attribut "aria-invalid" à true
                inputRppsId.setAttribute('aria-invalid', 'true');

                // Je mets l'attribut "hidden" de mon message d'erreur à faux,
                // car je ne peux pas utiliser les conditions par défaut de Keycloak,
                // je dois donc faire une logique ici, qui fait simplement en sorte qu'un élément HTML apparaisse si l'on parvient à cet endroit du code
                errorMessage.hidden = false;

                // J'empêche la soumission du formulaire en cas d'erreur
                event.preventDefault();
        }else{

            errorMessage.hidden = true;
            inputRppsId.setAttribute('aria-invalid', 'false');

        }
    });
});