# Create an optimized image
# see https://www.keycloak.org/server/containers
FROM quay.io/keycloak/keycloak:24.0 as builder

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true
ENV KC_DB=postgres

ADD --chown=keycloak:keycloak --chmod=644 ./import/providers/keycloak-prosanteconnect-4.0.0.jar /opt/keycloak/providers/keycloak-prosanteconnect-4.0.0.jar

RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:24.0
COPY --from=builder /opt/keycloak/ /opt/keycloak/

ENV KC_DB=postgres
ENV KC_DB_URL=<DBURL>
ENV KC_DB_USERNAME=<DBUSERNAME>
ENV KC_DB_PASSWORD=<DBPASSWORD>
ENV KC_HOSTNAME=localhost
ENV KC_LOG_LEVEL=info
ENV KC_HTTP_ENABLED=true
ENV KC_LOG=console
ENV KC_PROXY=edge
ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
